<footer id="footer" class="footer">
    <div class="container">
        <div class="footer__top">
            <div class="row">
                <div class="col-lg-5">
                    <div class="footer__top__info">
                        <a title="Logo" href="https://ezystayz.com/" class="footer__top__info__logo"><img src="{{asset(setting('logo') ? 'uploads/' . setting('logo') : 'assets/images/assets/logo.png')}}" alt="logo"></a>
                        <p class="footer__top__info__desc">{{__('Discover amazing things to do everywhere you go.')}}</p>
                        <!--<div class="footer__top__info__app">-->
                    <!--    <a title="App Store" href="#" class="banner-apps__download__iphone"><img src="{{asset('assets/images/assets/app-store.png')}}" alt="App Store"></a>-->
                    <!--    <a title="Google Play" href="#" class="banner-apps__download__android"><img src="{{asset('assets/images/assets/google-play.png')}}" alt="Google Play"></a>-->
                        <!--</div>-->
                    </div>
                </div>
                <div class="col-lg-2">
                    <aside class="footer__top__nav">
                        <h3>{{__('Company')}}</h3>
                        <ul>
                            <li><a href="https://ezystayz.com/about-us">{{__('About Us')}}</a></li>
                            <li><a href="{{route('post_list_all')}}">{{__('Blog')}}</a></li>
                            <li><a href="https://ezystayz.com/faq">{{__('Faqs')}}</a></li>
                            <li><a href="https://ezystayz.com/contact-us">{{__('Contact')}}</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-2">
                    <aside class="footer__top__nav">
                        <h3>{{__('Support')}}</h3>
                        <ul>
                            <li><a href="https://ezystayz.com/contact-us">Get in Touch</a></li>
                            <li><a href="https://ezystayz.com/help">Help Center</a></li>
                            <!--<li><a href="#">Live chat</a></li>-->
                            <li><a href="https://ezystayz.com">How it works</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3">
                    <aside class="footer__top__nav footer__top__nav--contact">
                        <h3>{{__('Contact Us')}}</h3>
                        <p>{{__('Email: info@ezystayz.com')}}</p>
                    <!--<p>{{__('Phone: +61 2 8310 4706')}}</p>-->
                        <ul>
                            <li>
                                <a title="Facebook" href="https://www.facebook.com/ezystayzofficial/">
                                    <i class="la la-facebook la-24"></i>
                                </a>
                            </li>
                            <li>
                                <a title="Twitter" href="https://twitter.com/ezystayz2014">
                                    <i class="la la-twitter la-24"></i>
                                </a>
                            </li>
                            <li>
                                <a title="Youtube" href="https://www.youtube.com/channel/UCDvQOHa-9pwh_PEEsgjsFWQ">
                                    <i class="la la-youtube la-24"></i>
                                </a>
                            </li>
                            <li>
                                <a title="Instagram" href="https://www.instagram.com/ezystayz/">
                                    <i class="la la-instagram la-24"></i>
                                </a>
                            </li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div><!-- .top-footer -->
        <div class="footer__bottom">
            <p class="footer__bottom__copyright">{{now()->year}} &copy; <a href="{{__('https://ezystayz.com')}}" target="_blank">{{__('EzyStayz')}}</a>. {{__('All rights reserved.')}}</p>
        </div><!-- .top-footer -->
    </div><!-- .container -->
</footer><!-- site-footer -->
